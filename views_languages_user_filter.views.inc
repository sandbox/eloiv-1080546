<?php
function views_languages_user_filter_views_handlers() {
    return array(
        'handlers' => array(
            'views_languages_user_filter' => array(
                'parent' => 'views_handler_filter',
            ),
        ),
    );
}

function views_languages_user_filter_views_data_alter(&$data) {
  $data['users']['users_languages'] = array(
  'real field' => 'users_languages',
  'title' => t('Users Languages'),
  'title short' => t('Users Languages'),
        'help' => t('This filter allows nodes to be filtered by languages selected in profile of user.'),
        'filter' => array(
            'field' => 'users_languages',
            'name table' => 'users_languages',
            'handler' => 'views_languages_user_filter',
        ),
    );
  return $data;
}
