<?php

/* Create filter of in views*/
class views_languages_user_filter extends views_handler_filter {

  function query() {
    global $user;
    if ($user->uid > 0) {
      $users_languages = db_fetch_object(db_query('SELECT * FROM {users_languages} WHERE uid = "%d" ', $user->uid));
      if ($users_languages) {
        $users_languages = unserialize($users_languages->languages);
        foreach ($users_languages as $key => $languages) {
          if ($languages !='0') {
            $lang[] = $languages;
          }
        }
        if (count($lang)==0) {
          $lang[] = '***CURRENT_LANGUAGE***';
        }
        $string_languages = implode("','", $lang);
      }
      else {
        $string_languages = '***CURRENT_LANGUAGE***';
      }
    }
    else {
      $string_languages = '***CURRENT_LANGUAGE***';
  }

  $this->query->add_where($this->options['group'], "node.language in ('" . $string_languages . "')");
  }
}
