INSTALL
-------
Navigate to administer >> build >> modules. Enable Views_languages_user_filter.

CONFIGURE
---------
Within the user settings, we find a list of languages, where the user 
can choose which are the languages you want to filter.

You will find a filter in the views filters. This filter, filter list views 
with languages actived profile user.

if you display form in user register, active this option in this configurate page.
admin/settings/views_languages_user_filter

MODULES REQUIRE
---------------
Views
i18n

